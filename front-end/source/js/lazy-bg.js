var lazyBg = (function(){
	var isLazedBg = function(){
		$('[data-lazy-bg]').each(function(index, el) {
			if(el.getBoundingClientRect().top < window.innerHeight + 200){
				$(this).css('background-image', 'url('+$(this).data('lazy-bg')+')');
			}
		});
	};
	var jaLazyBg = false
	setTimeout(isLazedBg(),200);

	$(window).scroll(function(){
		if(jaLazyBg) return;
		setTimeout(function(){
			jaLazyBg =false;
		},100);

		isLazedBg();
	});
})();