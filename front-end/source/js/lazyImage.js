var lazyImage = (function(){
	var isLazyPoint = function(){
		$('lazyimage').each(function(index, el) {
			if(el.getBoundingClientRect().top < window.innerHeight + 200){
				var classe = $(this).data('class') ? 'class="'+$(this).data('class')+'"' : '';
				$(this).html('<img src="'+$(this).data('src')+'" alt="'+$(this).data('alt')+'" '+classe+'/>').css('backgroundImage', 'none');
			}
		});
	};
	var jaLazyImage = false;

	setTimeout(isLazyPoint(),200);

	$(window).scroll(function(){
		if(jaLazyImage) return;

		setTimeout(function(){
			jaLazyImage = false
		},100);

		isLazyPoint();
	});
})();