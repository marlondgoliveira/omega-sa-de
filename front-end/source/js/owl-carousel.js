$('.carousel-servicos').owlCarousel({
	loop: true,
	autoplay:true,
	nav: true,
	navText: [
		'<i class="icone servico-prev"></i>',
		'<i class="icone servico-next"></i>'
	],
	responsive: {
		0: {
			items: 1,
		},
		600:{
			items : 2
		},
		992: {
			items: 2
		}
	}
});

$('#depoimentos').owlCarousel({
	nav: true,
	navText: [
		'<i class="icone depoimento-prev"></i>',
		'<i class="icone depoimento-next"></i>'
	],
	responsive: {
		0: {
			items: 1,
		},
		600:{
			items : 1
		},
		992: {
			items: 1
		}
	}
});
